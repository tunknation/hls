﻿using System;
using System.Linq;
using HLS.Domain.SeedWork;

namespace HLS.UnitTests.SeedWork {
    public abstract class EntityTestBase {
        protected static void AssertPublishedDomainEvent<T>(Entity aggregate) where T : IDomainEvent {
            var domainEvent = DomainEventsTestHelper.GetAllDomainEvents(aggregate).OfType<T>().SingleOrDefault();

            if (domainEvent is null) {
                throw new Exception($"{typeof(T).Name} event not published");
            }
        }
    }
}