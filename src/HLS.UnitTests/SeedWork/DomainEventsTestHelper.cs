﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using HLS.Domain.SeedWork;

namespace HLS.UnitTests.SeedWork {
    public static class DomainEventsTestHelper {
        public static IEnumerable<IDomainEvent> GetAllDomainEvents(Entity aggregate) {
            var domainEvents = new List<IDomainEvent>();

            domainEvents.AddRange(aggregate.GetDomainEvents());

            var memberInfo = aggregate.GetType().BaseType;
            if (memberInfo is null) {
                return domainEvents;
            }

            var fields = aggregate.GetType()
                .GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public)
                .Concat(memberInfo.GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public))
                .ToArray();

            foreach (var field in fields) {
                var isEntity = typeof(Entity).IsAssignableFrom(field.FieldType);

                if (isEntity) {
                    var entity = field.GetValue(aggregate) as Entity;
                    domainEvents.AddRange(GetAllDomainEvents(entity).ToList());
                }

                if (field.FieldType == typeof(string) || !typeof(IEnumerable).IsAssignableFrom(field.FieldType)) {
                    continue;
                }

                if (field.GetValue(aggregate) is not IEnumerable enumerable) {
                    continue;
                }

                foreach (var en in enumerable) {
                    if (en is Entity entityItem) {
                        domainEvents.AddRange(GetAllDomainEvents(entityItem));
                    }
                }
            }

            return domainEvents;
        }
    }
}