﻿using System;
using FluentAssertions;
using HLS.Domain.Functional;
using Xunit;

namespace HLS.UnitTests.Functional {
    public class EitherTests {
        [Fact]
        public void Either_WhenValueExist_ReturnsThatValue() {
            const bool val = true;
            Either<Exception, bool> either = val;

            var result = either.Value<bool>();

            result.Should().Be(val);
        }

        [Fact]
        public void Either_WhenValueDoesNotExist_ThrowsException() {
            Either<Exception, bool> either = true;
            Func<object> testCode = () => either.Value<int>();

            testCode.Should().Throw<Exception>();
        }

        [Fact]
        public void Either_WhenTypeIsValue_ReturnsTrue() {
            Either<Exception, int> either = 7;

            var result = either.IsValue<int>();

            result.Should().Be(true);
        }

        [Fact]
        public void Either_WhenTypeIsNotValue_ReturnsFalse() {
            Either<Exception, int> either = 7;

            var result = either.IsValue<Exception>();

            result.Should().Be(false);
        }
    }
}