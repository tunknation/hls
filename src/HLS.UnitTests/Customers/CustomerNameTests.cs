﻿using FluentAssertions;
using HLS.Domain.Customers;
using HLS.Domain.Functional;
using HLS.UnitTests.EqualityTests;
using Xunit;

namespace HLS.UnitTests.Customers {
    public class CustomerNameTests {
        private static CustomerName CustomerName1 => CustomerName.Of("Steve").Value<CustomerName>();
        private static CustomerName CustomerName2 => CustomerName.Of("Per").Value<CustomerName>();
        
        [Fact]
        public void CustomerName_ImplementsObjectEquality() {
            EqualityTest
                .ForValueObject(CustomerName1)
                .EqualTo(CustomerName1)
                .NotEqualTo(CustomerName2, "different name")
                .Assert();
        }

        [Fact]
        public void CustomerName_WhenEmpty_ReturnsError() {
            var isError = CustomerName
                .Of("")
                .Map(_ => false)
                .Reduce(_ => true);

            isError.Should().Be(true);
        }
    }
}