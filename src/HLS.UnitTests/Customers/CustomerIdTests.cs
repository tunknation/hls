﻿using System;
using HLS.Domain.Customers;
using HLS.UnitTests.EqualityTests;
using Xunit;

namespace HLS.UnitTests.Customers {
    public class CustomerIdTests {
        private static CustomerId CustomerId1 => CustomerId.Of(Guid.Parse("33d5306e-dfa2-4856-be3a-2ce67c546aa7"));
        private static CustomerId CustomerId2 => CustomerId.Of(Guid.Parse("8015be48-7243-48a6-91e1-7acf54285de5"));

        [Fact]
        public void CustomerId_ImplementsObjectEquality() {
            EqualityTest
                .ForTypedId(CustomerId1)
                .EqualTo(CustomerId1)
                .NotEqualTo(CustomerId2, "different id")
                .Assert();
        }
    }
}