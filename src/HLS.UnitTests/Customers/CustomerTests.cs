﻿using HLS.Domain.Customers;
using HLS.Domain.Customers.Events;
using HLS.Domain.Functional;
using HLS.Domain.SharedKernel;
using HLS.UnitTests.EqualityTests;
using HLS.UnitTests.SeedWork;
using Xunit;

namespace HLS.UnitTests.Customers {
    public class CustomerEntityTests : EntityTestBase {
        [Fact]
        public void Customer_ImplementsObjectEquality() {
            var customer1 = _registerNewCustomer();
            var customer2 = _registerNewCustomer();
            
            EqualityTest
                .ForEntity(customer1)
                .EqualTo(customer1)
                .NotEqualTo(customer2, "different customer")
                .Assert();
        }
        
        [Fact]
        public void RegisterNewCustomer_IsSuccessful() {
            var customer = _registerNewCustomer();

            AssertPublishedDomainEvent<NewCustomerRegisteredDomainEvent>(customer);
        }

        private static Customer _registerNewCustomer() {
            var name = CustomerName.Of("Per").Value<CustomerName>();
            var email = Email.Of("per@test.com").Value<Email>();
            var phoneNumber = PhoneNumber.Of("99999999").Value<PhoneNumber>();
            var contactInfo = CustomerContactInfo.Of(email, phoneNumber).Value<CustomerContactInfo>();
            var address = None.Value;

            return Customer.RegisterNewCustomer(name, contactInfo, address);
        }
    }
}