﻿using FluentAssertions;
using HLS.Domain.Customers;
using HLS.Domain.Functional;
using HLS.Domain.SharedKernel;
using HLS.UnitTests.EqualityTests;
using Xunit;

namespace HLS.UnitTests.Customers {
    public class CustomerContactInfoTests {
        private static Email Email1 => Email.Of("steve@test.com").Value<Email>();
        private static Email Email2 => Email.Of("per@test.com").Value<Email>();
        private static PhoneNumber PhoneNumber1 => PhoneNumber.Of("99999999").Value<PhoneNumber>();
        private static PhoneNumber PhoneNumber2 => PhoneNumber.Of("88888888").Value<PhoneNumber>();

        private static CustomerContactInfo CustomerContactInfo1 =>
            CustomerContactInfo.Of(Email1, PhoneNumber1).Value<CustomerContactInfo>();

        private static CustomerContactInfo CustomerContactInfo2 =>
            CustomerContactInfo.Of(Email2, PhoneNumber1).Value<CustomerContactInfo>();


        private static CustomerContactInfo CustomerContactInfo3 =>
            CustomerContactInfo.Of(Email1, PhoneNumber2).Value<CustomerContactInfo>();

        [Fact]
        public void CustomerContactInfo_ImplementsObjectEquality() {
            EqualityTest
                .ForValueObject(CustomerContactInfo1)
                .EqualTo(CustomerContactInfo1)
                .NotEqualTo(CustomerContactInfo2, "different email")
                .NotEqualTo(CustomerContactInfo3, "different phone number")
                .Assert();
        }

        [Fact]
        public void CustomerContactInfo_WhenEmailAndPhoneNumberAreNone_ReturnsError() {
            var error = CustomerContactInfo.Of(None.Value, None.Value).Value<Error>();

            error.Should().BeOfType<Error>();
        }
    }
}