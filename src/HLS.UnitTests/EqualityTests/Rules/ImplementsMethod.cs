﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace HLS.UnitTests.EqualityTests.Rules {
    internal abstract class ImplementsMethod<T> : ITestRule {
        private IEnumerable<MethodInfo> _targetMethod;

        protected ImplementsMethod(string methodName, params Type[] argumentTypes)
            : this(methodName, methodName, argumentTypes) { }

        protected ImplementsMethod(string methodName, string methodLabel, params Type[] argumentTypes) {
            MethodName = methodName;
            MethodLabel = methodLabel;
            ArgumentTypes = argumentTypes;

            DiscoverTargetMethod = () => {
                var method =
                    typeof(T).GetMethod(MethodName, argumentTypes) ?? 
                    typeof(T).BaseType?.GetMethod(MethodName, argumentTypes);

                _targetMethod = method is null ? Enumerable.Empty<MethodInfo>() : new[] { method };

                DiscoverTargetMethod = () => { };
            };
        }

        private string MethodName { get; }
        private string MethodLabel { get; }
        private Type[] ArgumentTypes { get; }
        private Action DiscoverTargetMethod { get; set; }

        private string OverrideLabel {
            get { return TryGetTargetMethod().Any(m => m.GetBaseDefinition() != null) ? "override" : "overload"; }
        }

        private string MethodSignature =>
            $"{MethodLabel}({string.Join(", ", ArgumentTypes.Select(type => type.Name))}";

        public IEnumerable<string> GetErrorMessages() {
            if (TryGetTargetMethod()
                .All(info => info.DeclaringType != typeof(T) && info.DeclaringType != typeof(T).BaseType)) {
                yield return $"{typeof(T).Name} should {OverrideLabel} {MethodSignature}).";
            }
        }

        public IEnumerable<MethodInfo> TryGetTargetMethod() {
            DiscoverTargetMethod();
            return _targetMethod;
        }
    }
}