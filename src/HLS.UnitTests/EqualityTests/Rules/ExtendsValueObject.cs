﻿using System.Collections.Generic;
using HLS.Domain.SeedWork;

namespace HLS.UnitTests.EqualityTests.Rules {
    public class ExtendsValueObject<T> : ITestRule {
        public IEnumerable<string> GetErrorMessages() {
            if (!typeof(T).IsSubclassOf(typeof(ValueObject))) {
                yield return $"{typeof(T).Name} should extend ${nameof(ValueObject)}.";
            }
        }
    }
}