﻿using System.Collections.Generic;
using HLS.Domain.SeedWork;

namespace HLS.UnitTests.EqualityTests.Rules {
    public class ExtendsEntity<T> : ITestRule {
        public IEnumerable<string> GetErrorMessages() {
            if (!typeof(T).IsSubclassOf(typeof(Entity))) {
                yield return $"{typeof(T).Name} should extend ${nameof(Entity)}.";
            }
        }
    }
}