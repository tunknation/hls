﻿using System.Collections.Generic;
using HLS.Domain.SeedWork;

namespace HLS.UnitTests.EqualityTests.Rules {
    public class ExtendsTypedId<T> : ITestRule {
        public IEnumerable<string> GetErrorMessages() {
            if (!typeof(T).IsSubclassOf(typeof(TypedId))) {
                yield return $"{typeof(T).Name} should extend ${nameof(TypedId)}.";
            }
        }
    }
}