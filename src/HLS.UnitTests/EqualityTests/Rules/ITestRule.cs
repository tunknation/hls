﻿using System.Collections.Generic;

namespace HLS.UnitTests.EqualityTests.Rules {
    internal interface ITestRule {
        IEnumerable<string> GetErrorMessages();
    }
}