﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace HLS.UnitTests.EqualityTests.Rules {
    internal class MethodReturns<T, TResult> : ITestRule {
        private MethodReturns(
            MethodInfo method,
            string methodLabel,
            T target,
            TResult result,
            string testCase,
            params object[] parameters) {
            Method = method;
            MethodLabel = methodLabel;
            TargetObject = target;
            ExpectedResult = result;
            MethodParameters = parameters;
            TestCase = testCase;
        }

        private MethodInfo Method { get; }
        private string MethodLabel { get; }
        private T TargetObject { get; }
        private TResult ExpectedResult { get; }
        private object[] MethodParameters { get; }
        private string TestCase { get; }

        public IEnumerable<string> GetErrorMessages() {
            IList<string> errors = new List<string>();

            try {
                var actualResult = (TResult)Method.Invoke(TargetObject, MethodParameters);
                if (actualResult != null && !actualResult.Equals(ExpectedResult)) {
                    errors.Add(
                        $"{Method.GetSignature(MethodLabel)} returned {actualResult} when expecting {ExpectedResult} - {TestCase}");
                }
            } catch (TargetInvocationException invocationExc) {
                errors.Add($"{Method.GetSignature()} failed - {invocationExc.Message}.");
            } catch (Exception exc) {
                errors.Add($"{Method.GetSignature()} failed - {exc.Message}.");
            }

            return errors;
        }

        public static MethodReturns<T, TResult> InstanceMethod(
            MethodInfo method, T target, TResult result,
            string testCase, params object[] parameters) {
            return new MethodReturns<T, TResult>(method, method.Name, target, result, testCase, parameters);
        }

        public static MethodReturns<T, TResult> Operator(
            MethodInfo method, string operatorLabel, T obj1, T obj2, TResult result, string testCase) {
            return new MethodReturns<T, TResult>(method, operatorLabel, default, result, testCase, obj1, obj2);
        }
    }
}