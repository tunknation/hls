﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace HLS.UnitTests.EqualityTests.Rules {
    internal class ImplementsIEquatable<T> : ITestRule {
        public IEnumerable<string> GetErrorMessages() {
            if (!typeof(T).GetInterfaces()
                .Any(i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IEquatable<>))) {
                yield return $"{typeof(T).Name} should implement IEquatable<{typeof(T).Name}>.";
            }
        }

        public IEnumerable<MethodInfo> TryGetTargetMethod() {
            var method =
                typeof(T).GetMethod("Equals", new[] { typeof(T) }) ??
                typeof(T).BaseType?.GetMethod("Equals", new[] { typeof(T) });

            if (method is null || method.GetParameters()[0].ParameterType != typeof(T)) {
                return Enumerable.Empty<MethodInfo>();
            }

            return new[] { method };
        }
    }
}