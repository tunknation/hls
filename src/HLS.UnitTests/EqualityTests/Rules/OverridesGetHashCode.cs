﻿namespace HLS.UnitTests.EqualityTests.Rules {
    internal class OverridesGetHashCode<T> : ImplementsMethod<T> {
        public OverridesGetHashCode() : base("GetHashCode") { }
    }
}