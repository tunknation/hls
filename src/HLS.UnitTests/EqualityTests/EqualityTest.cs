﻿using HLS.UnitTests.EqualityTests.Analysis;

namespace HLS.UnitTests.EqualityTests {
    public static class EqualityTest {
        public static EqualityTester<T> ForValueObject<T>(T obj) {
            var analysis = ValueObjectTypeAnalysis<T>.Analyze();

            return new EqualityTester<T>(obj, analysis);
        }

        public static EqualityTester<T> ForTypedId<T>(T obj) {
            var analysis = TypedIdTypeAnalysis<T>.Analyze();

            return new EqualityTester<T>(obj, analysis);
        }

        public static EqualityTester<T> ForEntity<T>(T obj) {
            var analysis = EntityTypeAnalysis<T>.Analyze();

            return new EqualityTester<T>(obj, analysis);
        }
    }
}