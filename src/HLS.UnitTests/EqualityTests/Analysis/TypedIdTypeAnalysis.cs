﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using HLS.UnitTests.EqualityTests.Rules;

namespace HLS.UnitTests.EqualityTests.Analysis {
    internal class TypedIdTypeAnalysis<T> : ITypeAnalysis<T> {
        private readonly IList<ITestRule> _rules = new List<ITestRule>();
        private IEnumerable<MethodInfo> _equalityOperator;
        private IEnumerable<MethodInfo> _equalsMethod;
        private IEnumerable<MethodInfo> _getHashCodeMethod;
        private IEnumerable<MethodInfo> _inequalityOperator;
        private IEnumerable<MethodInfo> _strongEqualsMethod;

        public IEnumerable<ITestRule> TypeLevelRules => _rules.ToList();

        private TypedIdTypeAnalysis() { }

        public static TypedIdTypeAnalysis<T> Analyze() {
            var analysis = new TypedIdTypeAnalysis<T>();

            AnalyzeTypedId(analysis);
            AnalyzeSealed(analysis);
            AnalyzeEquatable(analysis);
            AnalyzeEquals(analysis);
            AnalyzeEqualityOperator(analysis);
            AnalyzeInequalityOperator(analysis);
            AnalyzeGetHashCode(analysis);

            return analysis;
        }

        private static void AnalyzeTypedId(TypedIdTypeAnalysis<T> analysis) {
            analysis._rules.Add(new ExtendsTypedId<T>());
        }

        private static void AnalyzeSealed(TypedIdTypeAnalysis<T> analysis) {
            analysis._rules.Add(new IsTypeSealed<T>());
        }

        private static void AnalyzeEquatable(TypedIdTypeAnalysis<T> analysis) {
            var rule = new ImplementsIEquatable<T>();
            analysis._strongEqualsMethod = rule.TryGetTargetMethod();
            analysis._rules.Add(rule);
        }

        private static void AnalyzeEquals(TypedIdTypeAnalysis<T> analysis) {
            ImplementsMethod<T> rule = new OverridesEquals<T>();
            analysis._equalsMethod = rule.TryGetTargetMethod();
            analysis._rules.Add(rule);
        }

        private static void AnalyzeEqualityOperator(TypedIdTypeAnalysis<T> analysis) {
            ImplementsMethod<T> rule = new OverloadsEqualityOperator<T>();
            analysis._equalityOperator = rule.TryGetTargetMethod();
            analysis._rules.Add(rule);
        }

        private static void AnalyzeInequalityOperator(TypedIdTypeAnalysis<T> analysis) {
            ImplementsMethod<T> rule = new OverloadsInequalityOperator<T>();
            analysis._inequalityOperator = rule.TryGetTargetMethod();
            analysis._rules.Add(rule);
        }

        private static void AnalyzeGetHashCode(TypedIdTypeAnalysis<T> analysis) {
            ImplementsMethod<T> rule = new OverridesGetHashCode<T>();
            analysis._getHashCodeMethod = rule.TryGetTargetMethod();
            analysis._rules.Add(rule);
        }

        public IEnumerable<ITestRule> GetEqualToRules(T instance, T other) {
            const string testCase = "equal value objects";
            return _getStrongEqualsReturns(instance, other, true, testCase)
                .Concat(_getEqualsReturns(instance, other, true, testCase))
                .Concat(_getEqualityOperatorReturns(instance, other, true, testCase))
                .Concat(_getInequalityOperatorReturns(instance, other, false, testCase))
                .Concat(_getEqualGetHashCodeReturns(instance, other))
                .ToList();
        }

        public IEnumerable<ITestRule> GetNotEqualRules(T instance, T other, string testCase) {
            return
                _getStrongEqualsReturns(instance, other, false, testCase)
                    .Concat(_getEqualsReturns(instance, other, false, testCase))
                    .Concat(_getEqualityOperatorReturns(instance, other, false, testCase))
                    .Concat(_getInequalityOperatorReturns(instance, other, true, testCase))
                    .ToList();
        }

        private IEnumerable<ITestRule> _getStrongEqualsReturns(T instance, T other, bool result, string testCase) {
            return _strongEqualsMethod.Select(
                method => MethodReturns<T, bool>.InstanceMethod(method, instance, result, testCase, other));
        }

        private IEnumerable<ITestRule> _getEqualsReturns(T instance, T other, bool result, string testCase) {
            return _equalsMethod.Select(
                method => MethodReturns<T, bool>.InstanceMethod(method, instance, result, testCase, other));
        }

        private IEnumerable<ITestRule> _getEqualityOperatorReturns(T instance, T other, bool result, string testCase) {
            return _equalityOperator.Select(
                method => MethodReturns<T, bool>.Operator(method, "operator ==", instance, other, result, testCase));
        }

        private IEnumerable<ITestRule>
            _getInequalityOperatorReturns(T instance, T other, bool result, string testCase) {
            return _inequalityOperator.Select(
                method => MethodReturns<T, bool>.Operator(method, "operator !=", instance, other, result, testCase));
        }

        private IEnumerable<ITestRule> _getEqualGetHashCodeReturns(T instance, T other) {
            return _getHashCodeMethod.Select(
                method => new GetHashCodeEqualReturns<T>(method, instance, other));
        }
    }
}