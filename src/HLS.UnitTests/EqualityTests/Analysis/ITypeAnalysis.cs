﻿using System.Collections.Generic;
using HLS.UnitTests.EqualityTests.Rules;

namespace HLS.UnitTests.EqualityTests.Analysis {
    internal interface ITypeAnalysis<T> {
        IEnumerable<ITestRule> TypeLevelRules { get; }
        IEnumerable<ITestRule> GetEqualToRules(T instance, T other);
        IEnumerable<ITestRule> GetNotEqualRules(T instance, T other, string testCase);
    }
}