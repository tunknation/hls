﻿using System.Linq;
using System.Reflection;

namespace HLS.UnitTests.EqualityTests {
    internal static class MethodExtensions {
        public static string GetSignature(this MethodInfo method) {
            return GetSignature(method, method.Name);
        }

        public static string GetSignature(this MethodInfo method, string methodLabel) {
            return $"{methodLabel}({GetArgumentsListSignature(method)})";
        }

        private static string GetArgumentsListSignature(MethodInfo method) {
            return string.Join(", ", GetArgumentSignatures(method));
        }

        private static string[] GetArgumentSignatures(MethodInfo method) {
            return method.GetParameters()
                .Select(param => $"{param.ParameterType.Name}")
                .ToArray();
        }
    }
}