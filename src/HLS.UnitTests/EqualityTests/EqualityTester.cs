﻿using System;
using System.Collections.Generic;
using System.Linq;
using HLS.UnitTests.EqualityTests.Analysis;
using HLS.UnitTests.EqualityTests.Rules;

namespace HLS.UnitTests.EqualityTests {
    public class EqualityTester<T> {
        internal EqualityTester(T targetObject, ITypeAnalysis<T> analysis) {
            TargetObject = targetObject;
            Analysis = analysis;

            Rules.AddRange(Analysis.TypeLevelRules);

            if (typeof(T).IsValueType) {
                return;
            }

            Rules.AddRange(Analysis.GetNotEqualRules(TargetObject, default, "inequality to null"));
        }

        private T TargetObject { get; }
        private List<ITestRule> Rules { get; } = new();
        private ITypeAnalysis<T> Analysis { get; }

        public EqualityTester<T> EqualTo(T obj) {
            Rules.AddRange(Analysis.GetEqualToRules(TargetObject, obj));
            return this;
        }

        public EqualityTester<T> NotEqualTo(T obj, string testCase) {
            Rules.AddRange(Analysis.GetNotEqualRules(TargetObject, obj, testCase));
            return this;
        }

        public void Assert() {
            var errorMessages =
                Rules.SelectMany(rule => rule.GetErrorMessages()).ToList();

            if (!errorMessages.Any()) {
                return;
            }

            var message =
                "There were errors testing equality logic:\n" +
                string.Join(Environment.NewLine, errorMessages.ToArray());

            throw new ValueSemanticException(message);
        }
    }
}