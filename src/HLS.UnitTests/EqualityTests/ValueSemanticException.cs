﻿using System;

namespace HLS.UnitTests.EqualityTests {
    internal class ValueSemanticException : Exception {
        public ValueSemanticException() { }

        public ValueSemanticException(string message)
            : base(message) { }

        public ValueSemanticException(string message, Exception innerException)
            : base(message, innerException) { }
    }
}