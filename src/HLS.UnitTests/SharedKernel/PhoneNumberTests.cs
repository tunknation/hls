﻿using FluentAssertions;
using HLS.Domain.Functional;
using HLS.Domain.SharedKernel;
using HLS.UnitTests.EqualityTests;
using Xunit;

namespace HLS.UnitTests.SharedKernel {
    public class PhoneNumberTests {
        [Fact]
        public void PhoneNumber_ImplementsObjectEquality() {
            EqualityTest
                .ForValueObject(PhoneNumber.Of("47999999").Value<PhoneNumber>())
                .EqualTo(PhoneNumber.Of("47999999").Value<PhoneNumber>())
                .EqualTo(PhoneNumber.Of("004747999999").Value<PhoneNumber>())
                .EqualTo(PhoneNumber.Of("4747999999").Value<PhoneNumber>())
                .EqualTo(PhoneNumber.Of("+4747999999").Value<PhoneNumber>())
                .NotEqualTo(PhoneNumber.Of("88888888").Value<PhoneNumber>(), "different phone number")
                .Assert();
        }

        [Fact]
        public void PhoneNumber_WhenEmpty_ReturnsError() {
            var isError = PhoneNumber
                .Of("")
                .Map(_ => false)
                .Reduce(_ => true);

            isError.Should().Be(true);
        }

        [Theory]
        [InlineData("9")]
        [InlineData("12345678")]
        [InlineData("2345678901")]
        [InlineData("004823456789")]
        [InlineData("0047234567")]
        [InlineData("invalid_number")]
        public void PhoneNumber_WhenInInvalidFormat_ReturnsError(string value) {
            var isError = PhoneNumber
                .Of(value)
                .Map(_ => false)
                .Reduce(_ => true);

            isError.Should().Be(true);
        }
    }
}