﻿using FluentAssertions;
using HLS.Domain.Functional;
using HLS.Domain.SharedKernel;
using HLS.UnitTests.EqualityTests;
using Xunit;

namespace HLS.UnitTests.SharedKernel {
    public class AddressTests {
        private static City City1 => City.Of("Sarpsborg").Value<City>();
        private static City City2 => City.Of("Fredrikstad").Value<City>();
        private static PostalCode PostalCode1 => PostalCode.Of("1727").Value<PostalCode>();
        private static PostalCode PostalCode2 => PostalCode.Of("1605").Value<PostalCode>();

        private static StreetAddress StreetAddress1 =>
            StreetAddress.Of("Vannverksveien 34").Value<StreetAddress>();

        private static StreetAddress StreetAddress2 =>
            StreetAddress.Of("Traraveien 13").Value<StreetAddress>();

        [Fact]
        public void City_ImplementsObjectEquality() {
            EqualityTest
                .ForValueObject(City1)
                .EqualTo(City1)
                .NotEqualTo(City2, "different city")
                .Assert();
        }

        [Fact]
        public void City_WhenEmpty_ReturnsError() {
            var isError = City
                .Of("")
                .Map(_ => false)
                .Reduce(_ => true);

            isError.Should().Be(true);
        }

        [Fact]
        public void PostalCode_ImplementsObjectEquality() {
            EqualityTest
                .ForValueObject(PostalCode1)
                .EqualTo(PostalCode1)
                .NotEqualTo(PostalCode2, "different postal code")
                .Assert();
        }

        [Fact]
        public void PostalCode_WhenEmpty_ReturnsError() {
            var isError = PostalCode
                .Of("")
                .Map(_ => false)
                .Reduce(_ => true);

            isError.Should().Be(true);
        }

        [Theory]
        [InlineData("1")]
        [InlineData("12345")]
        [InlineData("12ab")]
        [InlineData("invalid_postal_code")]
        public void PostalCode_WhenInInvalidFormat_ReturnsError(string value) {
            var isError = PostalCode
                .Of(value)
                .Map(_ => false)
                .Reduce(_ => true);

            isError.Should().Be(true);
        }

        [Fact]
        public void StreetAddress_ImplementsObjectEquality() {
            EqualityTest
                .ForValueObject(StreetAddress1)
                .EqualTo(StreetAddress1)
                .NotEqualTo(StreetAddress2, "different street address")
                .Assert();
        }

        [Fact]
        public void StreetAddress_WhenEmpty_ReturnsError() {
            var isError = StreetAddress
                .Of("")
                .Map(_ => false)
                .Reduce(_ => true);

            isError.Should().Be(true);
        }

        [Fact]
        public void Address_ImplementsObjectEquality() {
            EqualityTest
                .ForValueObject(Address.Of(City1, PostalCode1, StreetAddress1))
                .EqualTo(Address.Of(City1, PostalCode1, StreetAddress1))
                .NotEqualTo(Address.Of(City2, PostalCode1, StreetAddress1), "different city")
                .NotEqualTo(Address.Of(City1, PostalCode2, StreetAddress1), "different postal code")
                .NotEqualTo(Address.Of(City1, PostalCode1, StreetAddress2), "different street address")
                .Assert();
        }
    }
}