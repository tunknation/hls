﻿using FluentAssertions;
using HLS.Domain.Functional;
using HLS.Domain.SharedKernel;
using HLS.UnitTests.EqualityTests;
using Xunit;

namespace HLS.UnitTests.SharedKernel {
    public class EmailTests {
        private static Email Email1 => Email.Of("steve@test.com").Value<Email>();
        private static Email Email2 => Email.Of("per@test.com").Value<Email>();
        
        [Fact]
        public void Email_ImplementsObjectEquality() {
            EqualityTest
                .ForValueObject(Email1)
                .EqualTo(Email1)
                .NotEqualTo(Email2, "different email")
                .Assert();
        }

        [Fact]
        public void Email_WhenEmpty_ReturnsError() {
            var isError = Email
                .Of("")
                .Map(_ => false)
                .Reduce(_ => true);

            isError.Should().Be(true);
        }

        [Theory]
        [InlineData("invalid")]
        [InlineData("test@")]
        [InlineData("@test")]
        public void Email_WhenInInvalidFormat_ReturnsError(string value) {
            var isError = Email
                .Of(value)
                .Map(_ => false)
                .Reduce(_ => true);

            isError.Should().Be(true);
        }
    }
}