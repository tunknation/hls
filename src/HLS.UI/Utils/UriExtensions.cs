﻿using System;
using System.Web;
using Microsoft.AspNetCore.WebUtilities;

namespace HLS.UI.Utils {
    public static class UriExtensions {
        public static Uri AddParameter(this Uri uri, string paramName, string paramValue) {
            if (uri.HasParameter(paramName)) {
                return new Uri(uri.ToString());
            }

            var uriBuilder = new UriBuilder(uri);
            var query = HttpUtility.ParseQueryString(uriBuilder.Query);

            query.Add(paramName, paramValue);
            uriBuilder.Query = query.ToString()!;

            return uriBuilder.Uri;
        }

        public static Uri RemoveParameter(this Uri uri, string paramName) {
            if (!uri.HasParameter(paramName)) {
                return new Uri(uri.ToString());
            }

            var uriBuilder = new UriBuilder(uri);
            var query = HttpUtility.ParseQueryString(uriBuilder.Query);

            query.Remove(paramName);
            uriBuilder.Query = query.ToString()!;

            return uriBuilder.Uri;
        }

        public static bool HasParameter(this Uri uri, string paramName) {
            return QueryHelpers.ParseQuery(uri.Query).TryGetValue(paramName, out _);
        }
    }
}