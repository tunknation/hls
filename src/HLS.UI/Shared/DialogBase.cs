﻿using System;
using HLS.UI.Utils;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Routing;

namespace HLS.UI.Shared {
    public abstract class DialogBase : ComponentBase, IDisposable {
        [Inject] private NavigationManager Nav { get; set; }

        protected string ParamName;
        protected bool Visibility;

        protected override void OnInitialized() {
            Nav.LocationChanged += _location;

            base.OnInitialized();
        }

        private void _location(object sender, LocationChangedEventArgs e) {
            var uri = Nav.ToAbsoluteUri(Nav.Uri);

            Visibility = Visibility switch {
                true when !uri.HasParameter(ParamName) => false,
                false when uri.HasParameter(ParamName) => true,
                _ => Visibility
            };

            StateHasChanged();
        }

        protected void OpenDialog() {
            Visibility = true;
        }

        protected void CloseDialog() {
            Visibility = false;
        }

        protected void OnDialogOpen() {
            var uri = Nav.ToAbsoluteUri(Nav.Uri);

            if (uri.HasParameter(ParamName)) {
                return;
            }

            uri = uri.AddParameter(ParamName, "true");
            Nav.NavigateTo(uri.ToString());
        }

        protected void OnDialogClose() {
            var uri = Nav.ToAbsoluteUri(Nav.Uri);

            if (!uri.HasParameter(ParamName)) {
                return;
            }

            uri = uri.RemoveParameter(ParamName);
            Nav.NavigateTo(uri.ToString());
        }

        public void Dispose() {
            Nav.LocationChanged -= _location;
        }
    }
}