using System;
using System.Net.Http;
using System.Threading.Tasks;
using HLS.UI.Customers.Services;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Syncfusion.Blazor;
using Syncfusion.Licensing;

namespace HLS.UI {
    public class Program {
        public static async Task Main(string[] args) {
            SyncfusionLicenseProvider
                .RegisterLicense(
                    "NTQ2MDgwQDMxMzkyZTMzMmUzMGNPZERuRThIalRXOXBVMkRINEh4aHZnRU0zbktXdkFVeXArRXovZ1BKVms9");

            var builder = WebAssemblyHostBuilder.CreateDefault(args);

            builder.RootComponents.Add<App>("#app");

            builder.Services.AddScoped(sp => new HttpClient
                { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });

            builder.Services
                .AddHlsClient()
                .ConfigureHttpClient(client =>
                    client.BaseAddress = new Uri("https://localhost:5001/graphql"));

            builder.Services.AddSyncfusionBlazor();
            builder.Services.AddCustomerServices();


            await builder.Build().RunAsync();
        }
    }
}