﻿using System;
using Microsoft.Extensions.DependencyInjection;

namespace HLS.UI.Customers.Services {
    public static class CustomerServices {
        public static void AddCustomerServices(this IServiceCollection services) {
            if (services is null) {
                throw new ArgumentNullException(nameof(services));
            }

            services.AddSingleton<CustomerRoutingService>();
        }
    }
}