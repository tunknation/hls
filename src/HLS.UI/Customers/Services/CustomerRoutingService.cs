﻿using System;
using Microsoft.AspNetCore.Components;

namespace HLS.UI.Customers.Services {
    public sealed class CustomerRoutingService {
        private readonly NavigationManager _nav;
        
        public CustomerRoutingService(NavigationManager nav) {
            _nav = nav;
        }

        public void NavigateToCustomerRegistrationDialog() {
            var uriBuilder = new UriBuilder(_nav.BaseUri) {
                Path = "customers",
                Query = "CustomerRegistrationDialog=true"
            };

            _nav.NavigateTo(uriBuilder.Uri.ToString());
        }
    }
}