﻿using System;
using HLS.Domain.SeedWork;

namespace HLS.Domain.Customers.Events {
    public sealed class NewCustomerRegisteredDomainEvent : DomainEventBase {
        public NewCustomerRegisteredDomainEvent(Guid customerId) {
            CustomerId = customerId;
        }

        public Guid CustomerId { get; }
    }
}