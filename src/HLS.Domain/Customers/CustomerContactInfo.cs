﻿using System.Collections.Generic;
using HLS.Domain.Functional;
using HLS.Domain.SeedWork;
using HLS.Domain.SharedKernel;

namespace HLS.Domain.Customers {
    public sealed class CustomerContactInfo : ValueObject {
        private CustomerContactInfo() {
            // EF Core
        }

        private CustomerContactInfo(Option<Email> email, Option<PhoneNumber> phoneNumber) {
            _email = email;
            _phoneNumber = phoneNumber;
        }

        private readonly Email? _email;
        public Option<Email> Email => _email;

        private readonly PhoneNumber? _phoneNumber;
        public Option<PhoneNumber> PhoneNumber => _phoneNumber;

        public static Either<Error, CustomerContactInfo> Of(Option<Email> email, Option<PhoneNumber> phoneNumber) {
            if (email is None<Email> && phoneNumber is None<PhoneNumber>) {
                return GeneralErrors.ValueIsInvalid("Email or Phone number is required!");
            }

            return new CustomerContactInfo(email, phoneNumber);
        }

        protected override IEnumerable<object?> GetEqualityComponents() {
            yield return _email;
            yield return _phoneNumber;
        }
    }
}