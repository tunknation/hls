﻿using HLS.Domain.Functional;
using HLS.Domain.SeedWork;
using HLS.Domain.SharedKernel;

namespace HLS.Domain.Customers {
    public sealed class CustomerName : ValueObject {
        private CustomerName() {
            // EF Core
        }
        
        private CustomerName(string value) {
            Value = value;
        }

        public string Value { get; }

        public static Either<Error, CustomerName> Of(string value) {
            value = value.Trim();

            if (value == string.Empty) {
                return GeneralErrors.ValueIsInvalid("Customer name cannot be empty");
            }

            return new CustomerName(value);
        }
    }
}