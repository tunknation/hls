﻿using HLS.Domain.Customers.Events;
using HLS.Domain.Functional;
using HLS.Domain.SeedWork;
using HLS.Domain.SharedKernel;

namespace HLS.Domain.Customers {
    public class Customer : Entity, IAggregateRoot {
        public override CustomerId Id { get; }

        private CustomerName Name { get; }
        private CustomerContactInfo ContactInfo { get; }

        private readonly Address? _address;
        private Option<Address> Address => _address;

        private Customer() {
            // EF Core
        }

        private Customer(
            CustomerId id,
            CustomerName name,
            CustomerContactInfo contactInfo,
            Option<Address> address) : base(id) {
            Id = id;
            Name = name;
            ContactInfo = contactInfo;
            _address = address;
        }

        public static Customer RegisterNewCustomer(
            CustomerName name,
            CustomerContactInfo contactInfo,
            Option<Address> address) {
            var id = CustomerId.New();

            var customer = new Customer(id, name, contactInfo, address);
            var @event = new NewCustomerRegisteredDomainEvent(id);

            customer.AddDomainEvent(@event);

            return customer;
        }
    }
}