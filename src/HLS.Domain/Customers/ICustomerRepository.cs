﻿using System.Threading.Tasks;
using HLS.Domain.Functional;

namespace HLS.Domain.Customers {
    public interface ICustomerRepository {
        Task AddAsync(Customer customer);

        Task<Option<Customer>> TryGetByIdAsync(CustomerId id);
    }
}