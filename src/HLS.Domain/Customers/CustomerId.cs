﻿using System;
using HLS.Domain.SeedWork;

namespace HLS.Domain.Customers {
    public sealed class CustomerId : TypedId {
        public CustomerId(Guid value) : base(value) { }

        public static CustomerId Of(Guid id) {
            return new CustomerId(id);
        }

        public static CustomerId New() {
            return new CustomerId(Guid.NewGuid());
        }
    }
}