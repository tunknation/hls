﻿using System;

namespace HLS.Domain.SeedWork {
    public abstract class TypedId : IEquatable<TypedId> {
        protected TypedId(Guid value) {
            if (value == Guid.Empty) {
                throw new InvalidOperationException("Id value cannot be empty!");
            }

            Value = value;
        }

        public Guid Value { get; }

        public bool Equals(TypedId? other) {
            return Value == other?.Value;
        }

        public override bool Equals(object? obj) {
            if (ReferenceEquals(null, obj)) {
                return false;
            }

            return obj is TypedId other && Equals(other);
        }

        public override int GetHashCode() {
            return Value.GetHashCode();
        }

        public static bool operator ==(TypedId? obj1, TypedId? obj2) {
            return obj1?.Equals(obj2) ?? Equals(obj2, null);
        }

        public static bool operator !=(TypedId? obj1, TypedId? obj2) {
            return !(obj1 == obj2);
        }

        public static implicit operator Guid(TypedId id) {
            return id.Value;
        }
    }
}