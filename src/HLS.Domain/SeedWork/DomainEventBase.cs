﻿using System;

namespace HLS.Domain.SeedWork {
    public abstract class DomainEventBase : IDomainEvent {
        protected DomainEventBase() {
            Id = Guid.NewGuid();
            OccurredOn = DateTime.UtcNow;
        }

        public Guid Id { get; }

        public DateTime OccurredOn { get; }
    }
}