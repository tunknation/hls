﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace HLS.Domain.SeedWork {
    public abstract class ValueObject : IEquatable<ValueObject> {
        private IEnumerable<PropertyInfo>? _properties;
        private IEnumerable<FieldInfo>? _fields;
        private IEnumerable<object?>? _equalityComponents;

        protected virtual IEnumerable<object?> GetEqualityComponents() {
            return _equalityComponents ??= _getProperties()
                .Select(p => p.GetValue(this, null))
                .Concat(_getFields().Select(f => f.GetValue(this)))
                .ToList();
        }

        public bool Equals(ValueObject? obj) {
            return Equals(obj as object);
        }
        
        public override bool Equals(object? obj) {
            if (obj is not ValueObject other) {
                return false;
            }

            if (GetType() != other.GetType()) {
                return false;
            }

            return GetEqualityComponents()
                .SequenceEqual(other.GetEqualityComponents());
        }

        public override int GetHashCode() {
            unchecked {
                var hash = 17;

                foreach (var value in GetEqualityComponents()) {
                    hash = _hashValue(hash, value);
                }

                return hash;
            }
        }

        public static bool operator ==(ValueObject? obj1, ValueObject? obj2) {
            return obj1?.Equals(obj2) ?? Equals(obj2, null);
        }

        public static bool operator !=(ValueObject? obj1, ValueObject? obj2) {
            return !(obj1 == obj2);
        }

        private IEnumerable<PropertyInfo> _getProperties() {
            return _properties ??= GetType()
                .GetProperties(BindingFlags.Instance | BindingFlags.Public)
                .Where(p => p.GetCustomAttribute(typeof(IgnoreMemberAttribute)) == null)
                .ToList();
        }

        private IEnumerable<FieldInfo> _getFields() {
            return _fields ??= GetType()
                .GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                .Where(p => p.GetCustomAttribute(typeof(IgnoreMemberAttribute)) == null)
                .ToList();
        }

        private static int _hashValue(int seed, object? value) {
            var currentHash = value?.GetHashCode() ?? 0;

            return seed * 23 + currentHash;
        }
    }
}