﻿using System;
using System.Collections.Generic;

namespace HLS.Domain.SeedWork {
    public abstract class Entity : IEquatable<Entity> {
        private readonly List<IDomainEvent> _domainEvents = new();

        protected Entity() {
            // EF Core
        }

        protected Entity(TypedId id) : this() {
            Id = id;
        }

        public virtual TypedId Id { get; }

        public bool Equals(Entity? obj) {
            return Equals(obj as object);
        }

        public IReadOnlyCollection<IDomainEvent> GetDomainEvents() {
            return _domainEvents.AsReadOnly();
        }

        protected void AddDomainEvent(IDomainEvent domainEvent) {
            _domainEvents.Add(domainEvent);
        }

        public void ClearDomainEvents() {
            _domainEvents.Clear();
        }

        public static bool operator ==(Entity? obj1, Entity? obj2) {
            return obj1?.Equals(obj2) ?? Equals(obj2, null);
        }

        public static bool operator !=(Entity? obj1, Entity? obj2) {
            return !(obj1 == obj2);
        }

        public override bool Equals(object? obj) {
            if (obj is not Entity other) {
                return false;
            }

            if (GetType() != other.GetType()) {
                return false;
            }
            
            if (ReferenceEquals(this, other)) {
                return true;
            }

            return Id == other.Id;
        }

        public override int GetHashCode() {
            return (GetType().ToString() + Id).GetHashCode();
        }
    }
}