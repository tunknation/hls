﻿using HLS.Domain.Functional;
using HLS.Domain.SeedWork;

namespace HLS.Domain.SharedKernel {
    public sealed class City : ValueObject {
        private City() {
            // EF Core
        }

        private City(string value) : this() {
            Value = value;
        }

        public string Value { get; }

        public static Either<Error, City> Of(string value) {
            value = value.Trim();

            if (value == string.Empty) {
                return GeneralErrors.ValueIsInvalid("City cannot be empty");
            }

            return new City(value);
        }
    }
}