﻿using HLS.Domain.Functional;
using HLS.Domain.SeedWork;

namespace HLS.Domain.SharedKernel {
    public sealed class StreetAddress : ValueObject {
        private StreetAddress() {
            // EF Core
        }

        private StreetAddress(string value) : this() {
            Value = value;
        }

        public string Value { get; }

        public static Either<Error, StreetAddress> Of(string value) {
            value = value.Trim();

            if (value == string.Empty) {
                return GeneralErrors.ValueIsInvalid("Street address cannot be empty");
            }

            return new StreetAddress(value);
        }
    }
}