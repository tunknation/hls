﻿using System.Text.RegularExpressions;
using HLS.Domain.Functional;
using HLS.Domain.SeedWork;

namespace HLS.Domain.SharedKernel {
    public sealed class Email : ValueObject {
        private static readonly Regex EmailRegex = new(@"^(.+)@(.+)$");

        private Email() {
            // EF Core
        }

        private Email(string value) : this() {
            Value = value;
        }

        public string Value { get; }

        public static Either<Error, Email> Of(string value) {
            value = value.Trim();

            if (value == string.Empty) {
                return GeneralErrors.ValueIsInvalid("Email cannot be empty");
            }

            if (!EmailRegex.IsMatch(value)) {
                return GeneralErrors.ValueIsInvalid("Email is in an invalid format");
            }

            return new Email(value);
        }
    }
}