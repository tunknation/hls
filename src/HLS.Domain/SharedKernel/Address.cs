﻿using HLS.Domain.SeedWork;

namespace HLS.Domain.SharedKernel {
    public sealed class Address : ValueObject {
        private Address() {
            // EF Core
        }

        private Address(City city, PostalCode postalCode, StreetAddress streetAddress) : this() {
            City = city;
            PostalCode = postalCode;
            StreetAddress = streetAddress;
        }

        public City City { get; }
        public PostalCode PostalCode { get; }
        public StreetAddress StreetAddress { get; }

        public static Address Of(City city, PostalCode postalCode, StreetAddress streetAddress) {
            return new Address(city, postalCode, streetAddress);
        }
    }
}