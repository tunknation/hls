﻿using System.Text.RegularExpressions;
using HLS.Domain.Functional;
using HLS.Domain.SeedWork;

namespace HLS.Domain.SharedKernel {
    public sealed class PhoneNumber : ValueObject {
        private static readonly Regex AreaCodeRegex = new(@"^(0047|\+47|47)");
        private static readonly Regex PhoneNumberRegex = new(@"^[2-9]\d{7}$");
        private static readonly Regex PhoneNumberWithAreaCodeRegex = new(@"^(0047|\+47|47)[2-9]\d{7}$");

        private PhoneNumber() {
            // EF Core
        }

        private PhoneNumber(string value) : this() {
            Value = value;
        }

        public string Value { get; }

        public static Either<Error, PhoneNumber> Of(string value) {
            value = value.Trim();
            value = _removeAreaCode(value);

            if (value == string.Empty) {
                return GeneralErrors.ValueIsInvalid("Phone number cannot be empty");
            }

            if (!PhoneNumberRegex.IsMatch(value)) {
                return GeneralErrors.ValueIsInvalid("Phone number is in an invalid format");
            }

            return new PhoneNumber(value);
        }

        private static string _removeAreaCode(string value) {
            if (PhoneNumberWithAreaCodeRegex.IsMatch(value)) {
                return AreaCodeRegex.Replace(value, "");
            }

            return value;
        }
    }
}