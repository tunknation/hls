﻿namespace HLS.Domain.SharedKernel {
    public static class GeneralErrors {
        public static Error ValueIsInvalid(string message) {
            return Error.Of("value.is.invalid", message);
        }
    }
}