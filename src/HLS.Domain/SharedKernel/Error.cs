﻿using System;
using System.Collections.Generic;
using HLS.Domain.SeedWork;

namespace HLS.Domain.SharedKernel {
    public sealed class Error : ValueObject {
        private const string Separator = "||";
        
        private Error(string code, string message) {
            Code = code;
            Message = message;
        }

        public static Error Of(string code, string message) {
            return new Error(code, message);
        }

        public string Code { get; }
        public string Message { get; }

        protected override IEnumerable<object?> GetEqualityComponents() {
            yield return Code;
        }

        public string Serialize() {
            return $"{Code}{Separator}{Message}";
        }

        public static Error Deserialize(string serialized) {
            var data = serialized.Split(new[] { Separator }, StringSplitOptions.RemoveEmptyEntries);

            if (data.Length != 2) {
                throw new Exception($"Invalid error serialization: '{serialized}'");
            }

            return new Error(data[0], data[1]);
        }
    }
}