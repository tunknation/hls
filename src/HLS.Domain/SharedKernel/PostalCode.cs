﻿using System.Text.RegularExpressions;
using HLS.Domain.Functional;
using HLS.Domain.SeedWork;

namespace HLS.Domain.SharedKernel {
    public sealed class PostalCode : ValueObject {
        private static readonly Regex PostalCodeRegex = new(@"^\d{4}$");

        private PostalCode() {
            // EF Core
        }

        private PostalCode(string value) : this() {
            Value = value;
        }

        public string Value { get; }

        public static Either<Error, PostalCode> Of(string value) {
            value = value.Trim();

            if (value == string.Empty) {
                return GeneralErrors.ValueIsInvalid("Postal code cannot be empty");
            }

            if (!PostalCodeRegex.IsMatch(value)) {
                return GeneralErrors.ValueIsInvalid("Postal code is in an invalid format");
            }

            return new PostalCode(value);
        }
    }
}