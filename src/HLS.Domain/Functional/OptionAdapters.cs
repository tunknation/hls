﻿using System;

namespace HLS.Domain.Functional {
    public static class OptionAdapters {
        public static Option<TResult> Map<T, TResult>(this Option<T> option, Func<T, TResult> map) {
            return option is Some<T> some ? map(some) : None.Value;
        }

        public static Option<T> When<T>(this T value, Func<T, bool> predicate) {
            return predicate(value) ? value : None.Value;
        }

        public static T Reduce<T>(this Option<T> option, T whenNone) {
            return option is Some<T> some ? some : whenNone;
        }
    }
}