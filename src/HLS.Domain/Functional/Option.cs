﻿namespace HLS.Domain.Functional {
    public abstract class Option<T> {
        public static implicit operator Option<T>(T? value) {
            if (value is null) {
                return new None<T>();
            }

            return new Some<T>(value);
        }

        public static implicit operator T?(Option<T> option) {
            if (option is not Some<T> some) {
                return default;
            }

            return some;
        }

        public static implicit operator Option<T>(None none) {
            return new None<T>();
        }
    }

    public class Some<T> : Option<T> {
        private readonly T _content;

        public Some(T content) {
            _content = content;
        }

        public static implicit operator T(Some<T> value) {
            return value._content;
        }
    }

    public class None<T> : Option<T> { }

    public class None {
        private None() { }

        public static None Value { get; } = new();
    }
}