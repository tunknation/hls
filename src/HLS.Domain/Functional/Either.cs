﻿using System;

namespace HLS.Domain.Functional {
    public abstract class Either<TLeft, TRight> {
        public static implicit operator Either<TLeft, TRight>(TLeft obj) {
            return new Left<TLeft, TRight>(obj);
        }

        public static implicit operator Either<TLeft, TRight>(TRight obj) {
            return new Right<TLeft, TRight>(obj);
        }

        public abstract bool IsValue<T>();

        public abstract T Value<T>();
    }

    public class Left<TLeft, TRight> : Either<TLeft, TRight> {
        private readonly TLeft _value;

        public Left(TLeft value) {
            _value = value;
        }

        public static implicit operator TLeft(Left<TLeft, TRight> obj) {
            return obj._value;
        }

        public override bool IsValue<T>() {
            return _value is T;
        }

        public override T Value<T>() {
            if (_value is not T val) {
                throw new InvalidOperationException($"Value is not of type ${typeof(T)}");
            }

            return val;
        }
    }

    public class Right<TLeft, TRight> : Either<TLeft, TRight> {
        private readonly TRight _value;
        
        public Right(TRight value) {
            _value = value;
        }

        public static implicit operator TRight(Right<TLeft, TRight> obj) {
            return obj._value;
        }

        public override bool IsValue<T>() {
            return _value is T;
        }

        public override T Value<T>() {
            if (_value is not T val) {
                throw new InvalidOperationException($"Value is not of type ${typeof(T)}");
            }

            return val;
        }
    }
}