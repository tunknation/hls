﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HLS.Domain.Functional {
    public static class EnumerableExtensions {
        public static Option<T> FirstOrNone<T>(this IEnumerable<T> sequence, Func<T, bool> predicate) {
            return sequence.Where(predicate)
                .Select<T, Option<T>>(x => x)
                .DefaultIfEmpty(None.Value)
                .First();
        }
    }
}