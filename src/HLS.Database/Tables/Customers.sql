CREATE TABLE [app].[Customers]
(
    [Id]                      UNIQUEIDENTIFIER NOT NULL,
    [Name]                    VARCHAR(MAX)     NOT NULL,
    [ContactInfo_Email]       VARCHAR(MAX)     NULL,
    [ContactInfo_PhoneNumber] VARCHAR(MAX)     NULL,
    [Address_City]            VARCHAR(MAX)     NULL,
    [Address_PostalCode]      VARCHAR(MAX)     NULL,
    [Address_StreetAddress]   VARCHAR(MAX)     NULL,
    CONSTRAINT [PK_app_Customer_Id] PRIMARY KEY ([Id] ASC)
)
GO