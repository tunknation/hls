﻿using MediatR;

namespace HLS.Application.SeedWork {
    public interface IQueryHandler<in TQuery, TResult> : IRequestHandler<TQuery, TResult>
        where TQuery : IQuery<TResult> { }
}