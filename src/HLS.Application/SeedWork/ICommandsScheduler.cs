﻿using System.Threading.Tasks;

namespace HLS.Application.SeedWork {
    public interface ICommandsScheduler {
        Task EnqueueAsync(ICommand command);

        Task EnqueueAsync<T>(ICommand<T> command);
    }
}