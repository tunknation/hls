﻿using System;
using HLS.Domain.Functional;
using HLS.Domain.SharedKernel;
using MediatR;

namespace HLS.Application.SeedWork {
    public interface ICommand<TResult> : IRequest<Either<Error, TResult>> {
        Guid Id { get; }
    }

    public interface ICommand : IRequest<Either<Error, None>> {
        Guid Id { get; }
    }
}