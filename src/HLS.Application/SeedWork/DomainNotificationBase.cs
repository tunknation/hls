﻿using System;
using HLS.Domain.SeedWork;

namespace HLS.Application.SeedWork {
    public abstract class DomainNotificationBase<T> : IDomainEventNotification<T> where T : IDomainEvent {
        protected DomainNotificationBase(T domainEvent, Guid id) {
            Id = id;
            DomainEvent = domainEvent;
        }

        public Guid Id { get; }

        public T DomainEvent { get; }
    }
}