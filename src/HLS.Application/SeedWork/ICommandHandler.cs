﻿using HLS.Domain.Functional;
using HLS.Domain.SharedKernel;
using MediatR;

namespace HLS.Application.SeedWork {
    public interface ICommandHandler<in TCommand> : IRequestHandler<TCommand, Either<Error, None>>
        where TCommand : ICommand { }

    public interface ICommandHandler<in TCommand, TResult> : IRequestHandler<TCommand, Either<Error, TResult>>
        where TCommand : ICommand<TResult> { }
}