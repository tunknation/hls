﻿using System;
using MediatR;

namespace HLS.Application.SeedWork {
    public interface IDomainEventNotification<out TEventType> : IDomainEventNotification {
        TEventType DomainEvent { get; }
    }

    public interface IDomainEventNotification : INotification {
        Guid Id { get; }
    }
}