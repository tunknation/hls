﻿using MediatR;

namespace HLS.Application.SeedWork {
    public interface IQuery<out TResult> : IRequest<TResult> { }
}