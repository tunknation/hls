using System;
using FluentValidation;
using HLS.Domain.Functional;
using HLS.Domain.SeedWork;
using HLS.Domain.SharedKernel;

namespace HLS.Application.Validators {
    public static class ValueObjectValidators {
        public static IRuleBuilderOptions<T, TValue> MustBeValueObject<T, TValue, TValueObject>(
            this IRuleBuilder<T, TValue> ruleBuilder,
            Func<TValue, Either<Error, TValueObject>> factoryMethod) where TValueObject : ValueObject {
            return (IRuleBuilderOptions<T, TValue>)ruleBuilder.Custom((value, context) => {
                var result = factoryMethod(value);

                if (result.IsValue<Error>()) {
                    context.AddFailure(result.Value<Error>().Serialize());
                }
            });
        }
    }
}