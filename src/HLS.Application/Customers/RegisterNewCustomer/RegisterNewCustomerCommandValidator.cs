using FluentValidation;
using HLS.Application.Validators;
using HLS.Domain.Customers;
using HLS.Domain.Functional;
using HLS.Domain.SharedKernel;

namespace HLS.Application.Customers.RegisterNewCustomer {
    internal sealed class RegisterNewCustomerCommandValidator : AbstractValidator<RegisterNewCustomerCommand> {
        public RegisterNewCustomerCommandValidator() {
            RuleFor(x => x.Name)
                .MustBeValueObject(CustomerName.Of);

            RuleFor(x => x.ContactInfo)
                .SetValidator(new ContactInfoValidator());

            When(x => x.Address is Some<RegisterNewCustomerCommand.AddressDto>, () => {
                RuleFor(x => (RegisterNewCustomerCommand.AddressDto)x.Address)
                    .SetValidator(new AddressValidator());
            });
        }


        private sealed class ContactInfoValidator : AbstractValidator<RegisterNewCustomerCommand.ContactInfoDto> {
            public ContactInfoValidator() {
                When(x => x.Email is Some<string>, () => {
                    RuleFor(x => (string)x.Email)
                        .MustBeValueObject(Email.Of);
                });

                When(x => x.PhoneNumber is Some<string>, () => {
                    RuleFor(x => (string)x.PhoneNumber)
                        .MustBeValueObject(PhoneNumber.Of);
                });

                RuleFor(x => x)
                    .MustBeValueObject(x => {
                        var emailResult = x.Email
                            .Map(Email.Of)
                            .Reduce(null);
                        Option<Email> email = None.Value;
                        if (emailResult is not null && emailResult.IsValue<Email>()) {
                            email = emailResult.Value<Email>();
                        }

                        var phoneNumberResult = x.PhoneNumber
                            .Map(PhoneNumber.Of)
                            .Reduce(null);
                        Option<PhoneNumber> phoneNumber = None.Value;
                        if (phoneNumberResult is not null && phoneNumberResult.IsValue<PhoneNumber>()) {
                            phoneNumber = phoneNumberResult.Value<PhoneNumber>();
                        }

                        return CustomerContactInfo.Of(email, phoneNumber);
                    });
            }
        }

        private sealed class AddressValidator : AbstractValidator<RegisterNewCustomerCommand.AddressDto> {
            public AddressValidator() {
                RuleFor(x => x.City)
                    .MustBeValueObject(City.Of);

                RuleFor(x => x.PostalCode)
                    .MustBeValueObject(PostalCode.Of);

                RuleFor(x => x.StreetAddress)
                    .MustBeValueObject(StreetAddress.Of);
            }
        }
    }
}