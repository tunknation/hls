using System;
using System.Threading;
using System.Threading.Tasks;
using HLS.Application.SeedWork;
using HLS.Domain.Customers;
using HLS.Domain.Functional;
using HLS.Domain.SharedKernel;

namespace HLS.Application.Customers.RegisterNewCustomer {
    internal sealed class RegisterNewCustomerCommandHandler : ICommandHandler<RegisterNewCustomerCommand, Guid> {
        private readonly ICustomerRepository _repository;

        public RegisterNewCustomerCommandHandler(ICustomerRepository repository) {
            _repository = repository;
        }

        public async Task<Either<Error, Guid>> Handle(
            RegisterNewCustomerCommand cmd,
            CancellationToken cancellationToken) {
            var name = CustomerName.Of(cmd.Name).Value<CustomerName>();
            var email = cmd.ContactInfo.Email
                .Map(val => Email.Of(val).Value<Email>());
            var phoneNumber = cmd.ContactInfo.PhoneNumber
                .Map(val => PhoneNumber.Of(val).Value<PhoneNumber>());
            var contactInfo = CustomerContactInfo.Of(email, phoneNumber).Value<CustomerContactInfo>();
            var address = cmd.Address.Map(val => {
                var city = City.Of(val.City).Value<City>();
                var postalCode = PostalCode.Of(val.PostalCode).Value<PostalCode>();
                var streetAddress = StreetAddress.Of(val.StreetAddress).Value<StreetAddress>();

                return Address.Of(city, postalCode, streetAddress);
            });

            var customer = Customer.RegisterNewCustomer(
                name,
                contactInfo,
                address);

            await _repository.AddAsync(customer);

            return customer.Id.Value;
        }
    }
}