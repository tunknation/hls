using System;
using HLS.Application.SeedWork;
using HLS.Domain.Functional;

namespace HLS.Application.Customers.RegisterNewCustomer {
    public sealed class RegisterNewCustomerCommand : CommandBase<Guid> {
        public RegisterNewCustomerCommand(
            string name,
            ContactInfoDto contactInfo,
            Option<AddressDto> address) {
            Name = name;
            ContactInfo = contactInfo;
            Address = address;
        }

        public string Name { get; }
        public ContactInfoDto ContactInfo { get; }
        public Option<AddressDto> Address { get; }

        public sealed class ContactInfoDto {
            public Option<string> Email { get; init; }
            public Option<string> PhoneNumber { get; init; }
        }

        public sealed class AddressDto {
            public string City { get; init; }
            public string PostalCode { get; init; }
            public string StreetAddress { get; init; }
        }
    }
}