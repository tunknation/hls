﻿using System.Data;

namespace HLS.Application.Data {
    public interface ISqlConnectionFactory {
        IDbConnection GetOpenConnection();

        IDbConnection CreateNewConnection();

        string GetConnectionString();
    }
}