﻿using System.Threading.Tasks;
using HLS.Application.SeedWork;
using HLS.Domain.Functional;
using HLS.Domain.SharedKernel;

namespace HLS.Application.Contracts {
    public interface IModule {
        Task<Either<Error, TResult>> ExecuteCommandAsync<TResult>(ICommand<TResult> command);

        Task<Either<Error, None>> ExecuteCommandAsync(ICommand command);

        Task<TResult> ExecuteQueryAsync<TResult>(IQuery<TResult> query);
    }
}