﻿using Autofac;
using HLS.Application.Contracts;
using HLS.Infrastructure;

namespace HLS.Api {
    public class HlsAutofacModule : Module {
        protected override void Load(ContainerBuilder builder) {
            builder.RegisterType<HlsModule>()
                .As<IModule>()
                .InstancePerLifetimeScope();
        }
    }
}