﻿using System;
using HotChocolate;

namespace HLS.Api.GraphQL.Customers.RegisterNewCustomer {
    public sealed class RegisterNewCustomerSuccess : IRegisterNewCustomerPayload {
        private RegisterNewCustomerSuccess(Guid customerId) {
            Status = MutationStatus.Success;
            CustomerId = customerId;
        }

        public static IRegisterNewCustomerPayload Of(Guid customerId) {
            return new RegisterNewCustomerSuccess(customerId);
        }

        [GraphQLNonNullType] public MutationStatus Status { get; }

        [GraphQLNonNullType] public Guid CustomerId { get; }
    }
}