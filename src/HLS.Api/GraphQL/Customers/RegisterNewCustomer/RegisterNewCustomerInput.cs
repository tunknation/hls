﻿using HLS.Application.Customers.RegisterNewCustomer;
using HotChocolate;

namespace HLS.Api.GraphQL.Customers.RegisterNewCustomer {
    public sealed class RegisterNewCustomerInput {
        [GraphQLNonNullType] public string Name { get; init; }
        [GraphQLNonNullType] public RegisterNewCustomerContactInfoInput ContactInfo { get; init; }
        public RegisterNewCustomerAddressInput Address { get; init; }

        public static explicit operator RegisterNewCustomerCommand(RegisterNewCustomerInput input) {
            var contactInfo = (RegisterNewCustomerCommand.ContactInfoDto)input.ContactInfo;
            var address = (RegisterNewCustomerCommand.AddressDto)input.Address;

            return new RegisterNewCustomerCommand(
                input.Name,
                contactInfo,
                address);
        }

        public sealed class RegisterNewCustomerContactInfoInput {
            public string Email { get; init; }
            public string PhoneNumber { get; init; }

            public static explicit operator RegisterNewCustomerCommand.ContactInfoDto(
                RegisterNewCustomerContactInfoInput contactInfo) {
                return new RegisterNewCustomerCommand.ContactInfoDto {
                    Email = contactInfo.Email,
                    PhoneNumber = contactInfo.PhoneNumber
                };
            }
        }

        public sealed class RegisterNewCustomerAddressInput {
            [GraphQLNonNullType] public string City { get; init; }
            [GraphQLNonNullType] public string PostalCode { get; init; }
            [GraphQLNonNullType] public string StreetAddress { get; init; }

            public static explicit operator RegisterNewCustomerCommand.AddressDto(
                RegisterNewCustomerAddressInput address) {
                if (address is null) {
                    return null;
                }

                return new RegisterNewCustomerCommand.AddressDto {
                    City = address.City,
                    PostalCode = address.PostalCode,
                    StreetAddress = address.StreetAddress
                };
            }
        }
    }
}