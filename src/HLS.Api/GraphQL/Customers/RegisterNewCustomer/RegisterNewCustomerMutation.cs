﻿using System.Collections.Generic;
using System.Threading.Tasks;
using HLS.Application.Contracts;
using HLS.Application.Customers.RegisterNewCustomer;
using HLS.Domain.Functional;
using HLS.Domain.SharedKernel;
using HotChocolate.Types;

namespace HLS.Api.GraphQL.Customers.RegisterNewCustomer {
    [ExtendObjectType(Name = "Mutation")]
    public sealed class RegisterNewCustomerMutation {
        private readonly IModule _module;

        public RegisterNewCustomerMutation(IModule module) {
            _module = module;
        }

        public async Task<IRegisterNewCustomerPayload> RegisterNewCustomerAsync(RegisterNewCustomerInput input) {
            var cmd = (RegisterNewCustomerCommand)input;

            var result = await _module.ExecuteCommandAsync(cmd);

            return result
                .Map(RegisterNewCustomerSuccess.Of)
                .Reduce(err => RegisterNewCustomerFailure.Of(new List<Error> { err }));
        }
    }
}