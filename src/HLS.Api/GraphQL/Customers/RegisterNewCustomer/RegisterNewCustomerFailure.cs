﻿using System.Collections.Generic;
using HotChocolate;
using HotChocolate.Types;
using Error = HLS.Domain.SharedKernel.Error;

namespace HLS.Api.GraphQL.Customers.RegisterNewCustomer {
    public sealed class RegisterNewCustomerFailure : IRegisterNewCustomerPayload {
        private RegisterNewCustomerFailure(IEnumerable<Error> errors) {
            Status = MutationStatus.Failure;
            Errors = errors;
        }

        [GraphQLNonNullType]
        [GraphQLType(typeof(ListType<NonNullType<ErrorType>>))]
        public IEnumerable<Error> Errors { get; }

        [GraphQLNonNullType] public MutationStatus Status { get; }

        public static IRegisterNewCustomerPayload Of(IEnumerable<Error> errors) {
            return new RegisterNewCustomerFailure(errors);
        }
    }
}