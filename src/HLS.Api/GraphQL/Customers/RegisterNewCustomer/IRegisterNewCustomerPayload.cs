﻿using HotChocolate.Types;

namespace HLS.Api.GraphQL.Customers.RegisterNewCustomer {
    [UnionType("RegisterNewCustomerPayload")]
    public interface IRegisterNewCustomerPayload {
        MutationStatus Status { get; }
    }
}