﻿using HotChocolate.Types;

namespace HLS.Api.GraphQL {
    [ExtendObjectType(Name = "Query")]
    public sealed class HelloWorldQuery {
        public string GetHelloWorld() {
            return "Hello World!";
        }
    }
}