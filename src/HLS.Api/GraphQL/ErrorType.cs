﻿using HLS.Domain.SharedKernel;
using HotChocolate.Types;

namespace HLS.Api.GraphQL {
    public sealed class ErrorType : ObjectType<Error> {
        protected override void Configure(IObjectTypeDescriptor<Error> descriptor) {
            descriptor.BindFieldsExplicitly();

            descriptor.Field(f => f.Code);
            descriptor.Field(f => f.Message);
        }
    }
}