using Autofac;
using HLS.Api.GraphQL;
using HLS.Api.GraphQL.Customers.RegisterNewCustomer;
using HLS.Infrastructure;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Formatting.Compact;

namespace HLS.Api {
    public class Startup {
        private const string ConnectionString = "ConnectionString";

        private static ILogger _logger;
        private static ILogger _loggerForApi;
        private readonly IConfiguration _configuration;

        private readonly IWebHostEnvironment _env;

        public Startup(IWebHostEnvironment env) {
            _configureLogger();

            _env = env;
            _configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json")
                .AddUserSecrets<Startup>()
                .AddEnvironmentVariables("Hls_")
                .Build();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services) {
            services.AddCors();

            services.AddAuthorization();

            services
                .AddGraphQLServer()
                .AddQueryType(d => d.Name("Query"))
                .AddType<HelloWorldQuery>()
                .AddMutationType(d => d.Name("Mutation"))
                .AddType<RegisterNewCustomerMutation>()
                .AddType<RegisterNewCustomerSuccess>()
                .AddType<RegisterNewCustomerFailure>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env) {
            HlsStartup.Initialize(_configuration[ConnectionString], _logger);

            app.UseCors(builder =>
                builder
                    .AllowAnyOrigin()
                    .AllowAnyHeader()
                    .AllowAnyMethod());

            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            } else {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapGraphQL(); });
        }

        public void ConfigureContainer(ContainerBuilder containerBuilder) {
            containerBuilder.RegisterModule(new HlsAutofacModule());
        }

        private static void _configureLogger() {
            _logger = new LoggerConfiguration()
                .Enrich.FromLogContext()
                .WriteTo.Console(
                    outputTemplate:
                    "[{Timestamp:HH:mm:ss} {Level:u3}] [{Module}] [{Context}] {Message:lj}{NewLine}{Exception}")
                .WriteTo.File(new CompactJsonFormatter(), "logs/logs")
                .CreateLogger();

            _loggerForApi = _logger.ForContext("Module", "HlsWebAPI");

            _loggerForApi.Information("Logger configured");
        }
    }
}