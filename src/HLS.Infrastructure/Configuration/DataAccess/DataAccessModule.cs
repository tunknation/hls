﻿using Autofac;
using HLS.Application.Data;
using HLS.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Microsoft.Extensions.Logging;

namespace HLS.Infrastructure.Configuration.DataAccess {
    internal class DataAccessModule : Module {
        private readonly string _connectionString;

        private readonly ILoggerFactory _loggerFactory;

        internal DataAccessModule(string connectionString, ILoggerFactory loggerFactory) {
            _connectionString = connectionString;
            _loggerFactory = loggerFactory;
        }

        protected override void Load(ContainerBuilder builder) {
            builder.RegisterType<SqlConnectionFactory>()
                .As<ISqlConnectionFactory>()
                .WithParameter("connectionString", _connectionString)
                .InstancePerLifetimeScope();

            builder
                .Register(c => {
                    var dbContextOptionsBuilder = new DbContextOptionsBuilder<HlsContext>();
                    dbContextOptionsBuilder.UseSqlServer(_connectionString);

                    dbContextOptionsBuilder
                        .ReplaceService<IValueConverterSelector, TypedIdValueConverterSelector>();
                    
                    return new HlsContext(dbContextOptionsBuilder.Options, _loggerFactory);
                })
                .AsSelf()
                .As<DbContext>()
                .InstancePerLifetimeScope();

            var infrastructureAssembly = typeof(HlsContext).Assembly;

            builder.RegisterAssemblyTypes(infrastructureAssembly)
                .Where(type => type.Name.EndsWith("Repository"))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope()
                .FindConstructorsWith(new AllConstructorFinder());
        }
    }
}