﻿using System.Threading.Tasks;
using HLS.Infrastructure.Configuration.Mediation;
using HLS.Infrastructure.Configuration.Processing.Outbox;
using Quartz;

namespace HLS.Infrastructure.Configuration.Quartz.Jobs {
    [DisallowConcurrentExecution]
    public class ProcessOutboxJob : IJob {
        public async Task Execute(IJobExecutionContext context) {
            await CommandsExecutor.Execute(new ProcessOutboxCommand());
        }
    }
}