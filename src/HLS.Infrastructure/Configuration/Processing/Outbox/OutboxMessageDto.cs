﻿using System;

namespace HLS.Infrastructure.Configuration.Processing.Outbox {
    public class OutboxMessageDto {
        public Guid Id { get; init; }

        public string Type { get; init; }

        public string Data { get; init; }
    }
}