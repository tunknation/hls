﻿using System;
using System.Threading;
using System.Threading.Tasks;
using HLS.Application.SeedWork;
using HLS.Domain.Functional;
using HLS.Domain.SharedKernel;

namespace HLS.Infrastructure.Configuration.Processing {
    internal class UnitOfWorkCommandHandlerWithResultDecorator<T, TResult> : ICommandHandler<T, TResult>
        where T : ICommand<TResult> {
        private readonly ICommandHandler<T, TResult> _decorated;

        private readonly IUnitOfWork _unitOfWork;

        public UnitOfWorkCommandHandlerWithResultDecorator(
            ICommandHandler<T, TResult> decorated,
            IUnitOfWork unitOfWork) {
            _decorated = decorated;
            _unitOfWork = unitOfWork;
        }

        public async Task<Either<Error, TResult>> Handle(T command, CancellationToken cancellationToken) {
            var result = await _decorated.Handle(command, cancellationToken);

            Guid? internalCommandId = null;
            if (command is InternalCommandBase<TResult>) {
                internalCommandId = command.Id;
            }

            await _unitOfWork.CommitAsync(cancellationToken, internalCommandId);

            return result;
        }
    }
}