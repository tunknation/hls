﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using HLS.Application.Data;
using HLS.Application.SeedWork;
using HLS.Domain.Functional;
using HLS.Domain.SharedKernel;
using HLS.Infrastructure.Configuration.Mediation;
using Newtonsoft.Json;
using Polly;

namespace HLS.Infrastructure.Configuration.Processing.InternalCommands {
    internal class ProcessInternalCommandsCommandHandler : ICommandHandler<ProcessInternalCommandsCommand> {
        private readonly ISqlConnectionFactory _sqlConnectionFactory;

        public ProcessInternalCommandsCommandHandler(ISqlConnectionFactory sqlConnectionFactory) {
            _sqlConnectionFactory = sqlConnectionFactory;
        }

        public async Task<Either<Error, None>> Handle(ProcessInternalCommandsCommand command,
            CancellationToken cancellationToken) {
            var connection = _sqlConnectionFactory.GetOpenConnection();

            const string sql = @"
                SELECT [Cmd].[Id],
                       [Cmd].[Type],
                       [Cmd].[Data]
                FROM [app].[InternalCommands] AS [Cmd]
                WHERE [Cmd].[ProcessedDate] IS NULL
                ORDER BY [Cmd].[EnqueueDate]";

            var internalCommands = await connection.QueryAsync<InternalCommandDto>(sql);
            var internalCommandsList = internalCommands.AsList();

            var policy = Policy
                .Handle<Exception>()
                .WaitAndRetryAsync(new[] {
                    TimeSpan.FromSeconds(1),
                    TimeSpan.FromSeconds(2),
                    TimeSpan.FromSeconds(3)
                });

            const string sqlUpdateProcessedDate = @"
                UPDATE [app].[InternalCommands]
                SET [ProcessedDate] = @Date
                WHERE [Id] = @Id";

            const string sqlUpdateError = @"
                UPDATE [app].[InternalCommands]
                SET [ProcessedDate] = @Date,
                    [Error] = @Error
                WHERE [Id] = @Id";

            foreach (var internalCommand in internalCommandsList) {
                var result = await policy.ExecuteAndCaptureAsync(() => ProcessCommand(internalCommand));

                if (result.Outcome == OutcomeType.Failure) {
                    await connection.ExecuteScalarAsync(
                        sqlUpdateError,
                        new {
                            Date = DateTime.UtcNow,
                            Error = result.FinalException.ToString(),
                            internalCommand.Id
                        });
                } else {
                    await connection.ExecuteScalarAsync(
                        sqlUpdateProcessedDate,
                        new {
                            Date = DateTime.UtcNow,
                            internalCommand.Id
                        });
                }
            }

            return None.Value;
        }

        private static async Task ProcessCommand(InternalCommandDto internalCommand) {
            var type = Assemblies.Application.GetType(internalCommand.Type);
            dynamic commandToProcess = JsonConvert.DeserializeObject(internalCommand.Data, type!);

            await CommandsExecutor.Execute(commandToProcess);
        }
    }
}