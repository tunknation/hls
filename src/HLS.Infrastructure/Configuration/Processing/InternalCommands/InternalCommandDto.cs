﻿using System;

namespace HLS.Infrastructure.Configuration.Processing.InternalCommands {
    public class InternalCommandDto {
        public Guid Id { get; init; }

        public string Type { get; init; }

        public string Data { get; init; }
    }
}