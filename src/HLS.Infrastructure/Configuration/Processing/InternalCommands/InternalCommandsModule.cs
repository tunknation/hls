﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using HLS.Application.SeedWork;
using HLS.Infrastructure.InternalCommands;

namespace HLS.Infrastructure.Configuration.Processing.InternalCommands {
    internal class InternalCommandsModule : Module {
        private readonly BiDictionary<string, Type> _internalCommandsMap;

        public InternalCommandsModule(BiDictionary<string, Type> internalCommandsMap) {
            _internalCommandsMap = internalCommandsMap;
        }

        protected override void Load(ContainerBuilder builder) {
            CheckMappings();

            builder.RegisterType<InternalCommandsMapper>()
                .As<IInternalCommandsMapper>()
                .FindConstructorsWith(new AllConstructorFinder())
                .WithParameter("internalCommandsMap", _internalCommandsMap)
                .SingleInstance();
        }

        private void CheckMappings() {
            var internalCommands = Assemblies.Application
                .GetTypes()
                .Where(x => x.BaseType is not null &&
                            (
                                x.BaseType.IsGenericType &&
                                x.BaseType.GetGenericTypeDefinition() == typeof(InternalCommandBase<>) ||
                                x.BaseType == typeof(InternalCommandBase)))
                .ToList();

            var notMappedInternalCommands = new List<Type>();
            foreach (var internalCommand in internalCommands) {
                _internalCommandsMap.TryGetBySecond(internalCommand, out var name);

                if (name is null) {
                    notMappedInternalCommands.Add(internalCommand);
                }
            }

            if (notMappedInternalCommands.Any()) {
                throw new ApplicationException(
                    $"Internal Commands {notMappedInternalCommands.Select(x => x.FullName).Aggregate((x, y) => x + "," + y)} not mapped");
            }
        }
    }
}