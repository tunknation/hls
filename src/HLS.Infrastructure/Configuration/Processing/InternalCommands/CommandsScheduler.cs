﻿using System;
using System.Threading.Tasks;
using Dapper;
using HLS.Application.Data;
using HLS.Application.SeedWork;
using HLS.Infrastructure.Serialization;
using Newtonsoft.Json;

namespace HLS.Infrastructure.Configuration.Processing.InternalCommands {
    public class CommandsScheduler : ICommandsScheduler {
        private readonly ISqlConnectionFactory _sqlConnectionFactory;

        public CommandsScheduler(ISqlConnectionFactory sqlConnectionFactory) {
            _sqlConnectionFactory = sqlConnectionFactory;
        }

        public async Task EnqueueAsync(ICommand command) {
            var connection = _sqlConnectionFactory.GetOpenConnection();

            const string sqlInsert = @"
                INSERT INTO [app].[InternalCommands] 
                    ([Id], [EnqueueDate], [Type], [Data]) 
                VALUES (@Id, @EnqueueDate, @Type, @Data)";

            await connection.ExecuteAsync(sqlInsert, new {
                command.Id,
                EnqueueDate = DateTime.UtcNow,
                Type = command.GetType().FullName,
                Data = JsonConvert.SerializeObject(command, new JsonSerializerSettings {
                    ContractResolver = new AllPropertiesContractResolver()
                })
            });
        }

        public async Task EnqueueAsync<T>(ICommand<T> command) {
            var connection = _sqlConnectionFactory.GetOpenConnection();

            const string sqlInsert = @"
                INSERT INTO [app].[InternalCommands] 
                    ([Id], [EnqueueDate], [Type], [Data]) 
                VALUES (@Id, @EnqueueDate, @Type, @Data)";

            await connection.ExecuteAsync(sqlInsert, new {
                command.Id,
                EnqueueDate = DateTime.UtcNow,
                Type = command.GetType().FullName,
                Data = JsonConvert.SerializeObject(command, new JsonSerializerSettings {
                    ContractResolver = new AllPropertiesContractResolver()
                })
            });
        }
    }
}