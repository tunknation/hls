﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using HLS.Application.SeedWork;
using HLS.Domain.Functional;
using HLS.Domain.SharedKernel;

namespace HLS.Infrastructure.Configuration.Processing {
    internal class ValidationCommandHandlerWithResultDecorator<T, TResult> : ICommandHandler<T, TResult>
        where T : ICommand<TResult> {
        private readonly ICommandHandler<T, TResult> _decorated;

        private readonly IList<IValidator<T>> _validators;

        public ValidationCommandHandlerWithResultDecorator(
            IList<IValidator<T>> validators,
            ICommandHandler<T, TResult> decorated) {
            _validators = validators;
            _decorated = decorated;
        }

        public async Task<Either<Error, TResult>> Handle(T command, CancellationToken cancellationToken) {
            var errors = _validators
                .Select(v => v.Validate(command))
                .SelectMany(result => result.Errors)
                .Where(error => error is not null)
                .ToList();

            if (errors.Any()) {
                return Error.Deserialize(errors.First().ErrorMessage);
            }

            return await _decorated.Handle(command, cancellationToken);
        }
    }
}