﻿using System.Reflection;
using HLS.Application.SeedWork;

namespace HLS.Infrastructure.Configuration {
    internal static class Assemblies {
        public static readonly Assembly Application = typeof(InternalCommandBase<>).Assembly;
    }
}