﻿using System.Threading.Tasks;
using Autofac;
using HLS.Application.SeedWork;
using MediatR;

namespace HLS.Infrastructure.Configuration.Mediation {
    internal static class QueryExecutor {
        internal static async Task<TResult> ExecuteQueryAsync<TResult>(IQuery<TResult> query) {
            using var scope = CompositionRoot.BeginLifetimeScope();
            var mediator = scope.Resolve<IMediator>();

            return await mediator.Send(query);
        }
    }
}