﻿using System.Threading.Tasks;
using Autofac;
using HLS.Application.SeedWork;
using HLS.Domain.Functional;
using HLS.Domain.SharedKernel;
using MediatR;

namespace HLS.Infrastructure.Configuration.Mediation {
    internal static class CommandsExecutor {
        internal static async Task<Either<Error, None>> Execute(ICommand command) {
            using var scope = CompositionRoot.BeginLifetimeScope();
            var mediator = scope.Resolve<IMediator>();

            return await mediator.Send(command);
        }

        internal static async Task<Either<Error, TResult>> Execute<TResult>(ICommand<TResult> command) {
            using var scope = CompositionRoot.BeginLifetimeScope();
            var mediator = scope.Resolve<IMediator>();

            return await mediator.Send(command);
        }
    }
}