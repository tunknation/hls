﻿using System.Threading.Tasks;

namespace HLS.Infrastructure.DomainEvents {
    public interface IDomainEventsDispatcher {
        Task DispatchEventsAsync();
    }
}