﻿using System.Collections.Generic;
using HLS.Domain.SeedWork;

namespace HLS.Infrastructure.DomainEvents {
    public interface IDomainEventsAccessor {
        IReadOnlyCollection<IDomainEvent> GetAllDomainEvents();

        void ClearAllDomainEvents();
    }
}