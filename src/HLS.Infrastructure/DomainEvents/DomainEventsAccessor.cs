﻿using System.Collections.Generic;
using System.Linq;
using HLS.Domain.SeedWork;
using Microsoft.EntityFrameworkCore;

namespace HLS.Infrastructure.DomainEvents {
    public class DomainEventsAccessor : IDomainEventsAccessor {
        private readonly DbContext _context;

        public DomainEventsAccessor(DbContext context) {
            _context = context;
        }

        public IReadOnlyCollection<IDomainEvent> GetAllDomainEvents() {
            var domainEntities = _context.ChangeTracker
                .Entries<Entity>()
                .Where(x => x.Entity.GetDomainEvents().Any()).ToList();

            return domainEntities
                .SelectMany(x => x.Entity.GetDomainEvents())
                .ToList();
        }

        public void ClearAllDomainEvents() {
            var domainEntities = _context.ChangeTracker
                .Entries<Entity>()
                .Where(x => x.Entity.GetDomainEvents().Any()).ToList();

            domainEntities
                .ForEach(entity => entity.Entity.ClearDomainEvents());
        }
    }
}