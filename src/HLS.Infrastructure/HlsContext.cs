﻿using HLS.Domain.Customers;
using HLS.Infrastructure.Domain.Customers;
using HLS.Infrastructure.InternalCommands;
using HLS.Infrastructure.Outbox;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace HLS.Infrastructure {
    public class HlsContext : DbContext {
        private readonly ILoggerFactory _loggerFactory;

        public HlsContext(DbContextOptions options, ILoggerFactory loggerFactory)
            : base(options) {
            _loggerFactory = loggerFactory;
        }

        internal DbSet<InternalCommand> InternalCommands { get; set; }

        internal DbSet<OutboxMessage> OutboxMessages { get; set; }

        internal DbSet<Customer> Customers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            modelBuilder.ApplyConfiguration(new InternalCommandEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new OutboxMessageEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CustomerEntityTypeConfiguration());
        }
    }
}