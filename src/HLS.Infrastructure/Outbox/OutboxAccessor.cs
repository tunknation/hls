﻿using System.Threading.Tasks;

namespace HLS.Infrastructure.Outbox {
    internal class OutboxAccessor : IOutbox {
        private readonly HlsContext _context;

        internal OutboxAccessor(HlsContext context) {
            _context = context;
        }

        public void Add(OutboxMessage message) {
            _context.OutboxMessages.Add(message);
        }

        public Task Save() {
            return
                Task.CompletedTask; // Save is done automatically using EF Core Change Tracking mechanism during SaveChanges.
        }
    }
}