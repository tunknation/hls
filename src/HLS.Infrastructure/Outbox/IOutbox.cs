﻿using System.Threading.Tasks;

namespace HLS.Infrastructure.Outbox {
    public interface IOutbox {
        void Add(OutboxMessage message);

        Task Save();
    }
}