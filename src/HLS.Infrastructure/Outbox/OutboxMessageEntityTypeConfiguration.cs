﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HLS.Infrastructure.Outbox {
    internal class OutboxMessageEntityTypeConfiguration : IEntityTypeConfiguration<OutboxMessage> {
        public void Configure(EntityTypeBuilder<OutboxMessage> builder) {
            builder.ToTable("OutboxMessages", "hls");

            builder.HasKey(b => b.Id);
            builder.Property(b => b.Id).ValueGeneratedNever();
        }
    }
}