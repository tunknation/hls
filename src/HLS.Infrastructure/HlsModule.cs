﻿using System.Threading.Tasks;
using HLS.Application.Contracts;
using HLS.Application.SeedWork;
using HLS.Domain.Functional;
using HLS.Domain.SharedKernel;
using HLS.Infrastructure.Configuration.Mediation;

namespace HLS.Infrastructure {
    public class HlsModule : IModule {
        public async Task<Either<Error, TResult>>
            ExecuteCommandAsync<TResult>(ICommand<TResult> command) {
            return await CommandsExecutor.Execute(command);
        }

        public async Task<Either<Error, None>> ExecuteCommandAsync(ICommand command) {
            return await CommandsExecutor.Execute(command);
        }

        public async Task<TResult> ExecuteQueryAsync<TResult>(IQuery<TResult> query) {
            return await QueryExecutor.ExecuteQueryAsync(query);
        }
    }
}