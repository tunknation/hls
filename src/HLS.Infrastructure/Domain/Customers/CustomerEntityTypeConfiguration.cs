﻿#nullable enable
using HLS.Domain.Customers;
using HLS.Domain.SharedKernel;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HLS.Infrastructure.Domain.Customers {
    internal class CustomerEntityTypeConfiguration : IEntityTypeConfiguration<Customer> {
        public void Configure(EntityTypeBuilder<Customer> builder) {
            builder.ToTable("Customers", "app");

            builder.HasKey(k => k.Id);

            builder.Property<CustomerName>("Name")
                .HasColumnName("Name")
                .HasConversion(
                    p => p.Value,
                    p => CustomerName.Of(p).Value<CustomerName>());

            builder.OwnsOne<CustomerContactInfo>(
                "ContactInfo",
                p => {
                    p.Property<Email?>("_email")
                        .HasColumnName("ContactInfo_Email")
                        .HasConversion(
                            pp => pp == null ? null : pp.Value,
                            pp => pp == null ? null : Email.Of(pp).Value<Email>());

                    p.Property<PhoneNumber?>("_phoneNumber")
                        .HasColumnName("ContactInfo_PhoneNumber")
                        .HasConversion(
                            pp => pp == null ? null : pp.Value,
                            pp => pp == null ? null : PhoneNumber.Of(pp).Value<PhoneNumber>());
                });

            builder.OwnsOne<Address?>(
                "_address",
                p => {
                    p.Property<City>("City")
                        .HasColumnName("Address_City")
                        .HasConversion(
                            pp => pp.Value,
                            pp => City.Of(pp).Value<City>());

                    p.Property<PostalCode>("PostalCode")
                        .HasColumnName("Address_PostalCode")
                        .HasConversion(
                            pp => pp.Value,
                            pp => PostalCode.Of(pp).Value<PostalCode>());

                    p.Property<StreetAddress>("StreetAddress")
                        .HasColumnName("Address_StreetAddress")
                        .HasConversion(
                            pp => pp.Value,
                            pp => StreetAddress.Of(pp).Value<StreetAddress>());
                });
        }
    }
}