﻿using System.Threading.Tasks;
using HLS.Domain.Customers;
using HLS.Domain.Functional;
using Microsoft.EntityFrameworkCore;

namespace HLS.Infrastructure.Domain.Customers {
    internal class CustomerRepository : ICustomerRepository {
        private readonly HlsContext _context;

        public CustomerRepository(HlsContext context) {
            _context = context;
        }

        public async Task AddAsync(Customer customer) {
            await _context.Customers.AddAsync(customer);
        }

        public async Task<Option<Customer>> TryGetByIdAsync(CustomerId id) {
            var result = await _context.Customers.FirstOrDefaultAsync(x => x.Id == id);

            return result == null ? new None<Customer>() : new Some<Customer>(result);
        }
    }
}