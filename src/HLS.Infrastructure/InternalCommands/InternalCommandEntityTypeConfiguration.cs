﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HLS.Infrastructure.InternalCommands {
    internal class InternalCommandEntityTypeConfiguration : IEntityTypeConfiguration<InternalCommand> {
        public void Configure(EntityTypeBuilder<InternalCommand> builder) {
            builder.ToTable("InternalCommands", "hls");

            builder.HasKey(b => b.Id);
            builder.Property(b => b.Id).ValueGeneratedNever();
        }
    }
}