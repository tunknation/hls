﻿using System;
using Autofac;
using HLS.Infrastructure.Configuration;
using HLS.Infrastructure.Configuration.DataAccess;
using HLS.Infrastructure.Configuration.Logging;
using HLS.Infrastructure.Configuration.Mediation;
using HLS.Infrastructure.Configuration.Processing;
using HLS.Infrastructure.Configuration.Processing.InternalCommands;
using HLS.Infrastructure.Configuration.Processing.Outbox;
using HLS.Infrastructure.Configuration.Quartz;
using Serilog;
using Serilog.Extensions.Logging;

namespace HLS.Infrastructure {
    public static class HlsStartup {
        private static IContainer _container;

        public static void Initialize(string connectionString, ILogger logger) {
            var moduleLogger = logger.ForContext("Module", "HLS");

            ConfigureContainer(connectionString, moduleLogger);

            QuartzStartup.Initialize(moduleLogger);
        }

        public static void Stop() {
            QuartzStartup.StopQuartz();
        }

        private static void ConfigureContainer(string connectionString, ILogger logger) {
            var containerBuilder = new ContainerBuilder();

            containerBuilder.RegisterModule(new LoggingModule(logger));

            var loggerFactory = new SerilogLoggerFactory(logger);

            containerBuilder.RegisterModule(new DataAccessModule(connectionString, loggerFactory));
            containerBuilder.RegisterModule(new ProcessingModule());
            containerBuilder.RegisterModule(new MediatorModule());

            var domainNotificationsMap = new BiDictionary<string, Type>();
            containerBuilder.RegisterModule(new OutboxModule(domainNotificationsMap));

            var internalCommandsMap = new BiDictionary<string, Type>();
            containerBuilder.RegisterModule(new InternalCommandsModule(internalCommandsMap));

            containerBuilder.RegisterModule(new QuartzModule());

            _container = containerBuilder.Build();

            CompositionRoot.SetContainer(_container);
        }
    }
}