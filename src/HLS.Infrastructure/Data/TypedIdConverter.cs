﻿using System;
using HLS.Domain.SeedWork;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace HLS.Infrastructure.Data {
    public class TypedIdConverter<TTypedId> : ValueConverter<TTypedId, Guid> where TTypedId : TypedId {
        public TypedIdConverter(ConverterMappingHints mappingHints = null)
            : base(id => id.Value, value => Create(value), mappingHints) { }

        private static TTypedId Create(Guid id) {
            return Activator.CreateInstance(typeof(TTypedId), id) as TTypedId;
        }
    }
}