﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using HLS.Domain.SeedWork;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace HLS.Infrastructure.Data {
    public class TypedIdValueConverterSelector : ValueConverterSelector {
        private readonly ConcurrentDictionary<(Type ModelClrType, Type ProviderClrType), ValueConverterInfo> _converters
            = new();

        public TypedIdValueConverterSelector(ValueConverterSelectorDependencies dependencies)
            : base(dependencies) { }

        public override IEnumerable<ValueConverterInfo> Select(Type modelClrType, Type providerClrType = null) {
            var baseConverters = base.Select(modelClrType, providerClrType);
            foreach (var converter in baseConverters) {
                yield return converter;
            }

            var underlyingModelType = UnwrapNullableType(modelClrType);
            var underlyingProviderType = UnwrapNullableType(providerClrType);

            if (underlyingProviderType is not null && underlyingProviderType != typeof(Guid)) {
                yield break;
            }

            var isTypedIdValue = typeof(TypedId).IsAssignableFrom(underlyingModelType);
            if (!isTypedIdValue) {
                yield break;
            }

            var converterType = typeof(TypedIdConverter<>).MakeGenericType(underlyingModelType);

            yield return _converters.GetOrAdd((underlyingModelType, typeof(Guid)), _ => {
                return new ValueConverterInfo(
                    modelClrType,
                    typeof(Guid),
                    valueConverterInfo =>
                        (ValueConverter)Activator.CreateInstance(converterType, valueConverterInfo.MappingHints));
            });
        }

        private static Type UnwrapNullableType(Type type) {
            if (type is null) {
                return null;
            }

            return Nullable.GetUnderlyingType(type) ?? type;
        }
    }
}